package project1.android.tcd.kz.db;

import java.util.Date;

/**
 * Created by kadirova on 12/12/2016.
 */


public class LocalBestScores {

    //private variables
    int _id;
    //String _player_id;
    //String _player_name;
    String _subject;
    //int _task_id;
    int _score;
    //Date _created;

    // Empty constructor
    public LocalBestScores(){

    }
    // constructor

/*
    public LocalBestScores(int _id, String _subject, int _score, Date _created) {
        this._id = _id;
        this._subject = _subject;
        this._score = _score;
        this._created = _created;
    }

    public LocalBestScores(String _subject, int _score, Date _created) {
        this._subject = _subject;
        this._score = _score;
        this._created = _created;
    }
*/
    public LocalBestScores(String _subject, int _score) {
        this._subject = _subject;
        this._score = _score;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_subject() {
        return _subject;
    }

    public void set_subject(String _subject) {
        this._subject = _subject;
    }

    public int get_score() {
        return _score;
    }

    public void set_score(int _score) {
        this._score = _score;
    }
    /*
    public Date get_created() {
        return _created;
    }

    public void set_created(Date _created) {
        this._created = _created;
    }
    */
}