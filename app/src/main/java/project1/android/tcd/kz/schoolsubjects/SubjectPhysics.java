package project1.android.tcd.kz.schoolsubjects;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class SubjectPhysics extends AppCompatActivity {

    StaticEquilibrium rods;

    int iScore = 0;
    CountDownTimer cdt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_physics);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        //set title
        TextView tv_title = (TextView)findViewById(R.id.subject_title);
        tv_title.setText(message);

        //set task
        TextView tv_task_no = (TextView)findViewById(R.id.task_title);
        tv_task_no.setText("Task 1: Statics -> Equilibrium");

        // set timer
        final TextView tv_task_timer = (TextView)findViewById(R.id.task_timer);
        cdt2 = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                iScore = (int) millisUntilFinished / 1000;
                tv_task_timer.setText("seconds remaining: " + iScore);
            }

            public void onFinish() {
                iScore = 0;
                tv_task_timer.setText("time is up, looser!");
            }
        }.start();

        //add custom view
        FrameLayout frame2 = (FrameLayout) findViewById(R.id.rods);

        rods = new StaticEquilibrium(this);
        frame2.addView(rods);

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //TextView tv_task_status = (TextView)findViewById(R.id.task_status);
        //tv_task_status.setText(rectgrid.getSquareColors());

        //correct result found
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                rods.dragPivot(event);
                break;
        }

        System.out.println("rods.torque1=" + rods.torque1 + "    rods.torque2=" + rods.torque2);
        if(rods.torque1 ==  rods.torque2){
            TextView tv_task_score2 = (TextView)findViewById(R.id.task_score);
            tv_task_score2.setText("Your score: " + iScore);
            cdt2.cancel();

        }
        return true; //super.onTouchEvent(event);
    }

}
