package project1.android.tcd.kz.schoolsubjects;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    /** Called when the user clicks the Send button */
    /*
    public void sendMessage(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }
    */

    /** Called when the user clicks the Math button */
    public void showSubjectMath(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, SubjectMath.class);
        Button button = (Button) findViewById(R.id.button_math);
        String message = button.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }

    /** Called when the user clicks the Math button */
    public void showSubjectPhysics(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, SubjectPhysics.class);
        Button button = (Button) findViewById(R.id.button_physics);
        String message = button.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }

    /** Called when the user clicks the Math button */
    public void showMyBestScores(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, MyBestScores.class);
        Button button = (Button) findViewById(R.id.button_mybestscores);
        String message = button.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }
}
