package project1.android.tcd.kz.schoolsubjects;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import project1.android.tcd.kz.db.DatabaseHandler;
import project1.android.tcd.kz.db.LocalBestScores;

public class MyBestScores extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_best_scores);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        System.out.println("message=" + message);

        //Show best scores
        String best10scores = "";
        final DatabaseHandler db = new DatabaseHandler(this);
        List<LocalBestScores> scores = db.getScores();

        for (LocalBestScores scr : scores) {
            String log = "Id: "+ scr.get_id() +" ,Subject: " + scr.get_subject() + ", Score:" + scr.get_score() ;
            // Writing Contacts to log
            Log.d("Name: ", log);
            best10scores += "Subject: " + scr.get_subject() + ", Score:" + scr.get_score() + "\n";
        }
        TextView tv_title = (TextView)findViewById(R.id.bestscores);
        tv_title.setText(best10scores);


        final Button button = (Button) findViewById(R.id.button_clear_scores);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Perform action on click
                db.deleteContact();
                finish();
                startActivity(getIntent());
            }
        });
    }

    @Override
    public void onClick(View v) {
        // default method for handling onClick Events..
    }




}
