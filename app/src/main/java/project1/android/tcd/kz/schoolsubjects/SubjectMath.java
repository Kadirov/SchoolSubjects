package project1.android.tcd.kz.schoolsubjects;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import project1.android.tcd.kz.db.DatabaseHandler;
import project1.android.tcd.kz.db.LocalBestScores;

public class SubjectMath extends AppCompatActivity {

    RectangleGrid rectgrid;
    int iScore = 0;
    int result;
    CountDownTimer cdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_math);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        //set title
        TextView tv_title = (TextView)findViewById(R.id.subject_title);
        tv_title.setText(message);

        //set task
        TextView tv_task_no = (TextView)findViewById(R.id.task_title);
        tv_task_no.setText("Task 1: Arithmetics");

        //set task text
        Random rand = new Random();
        int x = rand.nextInt(11); // Gives n such that 0 <= n <
        int y = rand.nextInt(11); // Gives n such that 0 <= n < 20

        char opearations[] = {'+','-','x','/'};
        int numOp = opearations.length;
        int operation = rand.nextInt(numOp + 1);

        try{
            switch (operation - 1){
                case -1: result = x + y; break;
                case 0: result = x + y; break;
                case 1: result = x - y; break;
                case 2: result = x * y; break;
                case 3: result = x / y; break;
                default: result = -1;
            }
        }catch (ArithmeticException ae){
            result = -2; //TODO: handle division by 0
        }

        TextView tv_task_text = (TextView)findViewById(R.id.task_text);
        tv_task_text.setText("\nCalculate expression: " + x + " " + opearations[operation == 0? operation : operation -1] + " " + y + " = ??? (" + result + ")");
        //+ "\nthen obtain squares of the same color by clicking on sqaures to win");

        // set timer
        final TextView tv_task_timer = (TextView)findViewById(R.id.task_timer);
        cdt = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                iScore = (int) millisUntilFinished / 1000;
                tv_task_timer.setText("seconds remaining: " + iScore);
            }

            public void onFinish() {
                iScore = 0;
                tv_task_timer.setText("time is up, looser!");

                //show popup window
                popupBox();
            }
        }.start();


        //Rectangular grid
        //RectangleGrid rectgrid;
        //rectgrid = (RectangleGrid)findViewById(R.id.rectgrid);
        FrameLayout frame = (FrameLayout) findViewById(R.id.rectgrid);

        rectgrid = new RectangleGrid(this);
        frame.addView(rectgrid);


        TextView tv_task_status = (TextView)findViewById(R.id.task_status);
        tv_task_status.setText(rectgrid.getSquareColors());

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        TextView tv_task_status = (TextView)findViewById(R.id.task_status);
        tv_task_status.setText(rectgrid.getSquareColors());

        //correct result found
        if(rectgrid.squareColors[0] == result || rectgrid.squareColors[1] == result){
            TextView tv_task_score = (TextView)findViewById(R.id.task_score);
            tv_task_score.setText("Your score: " + iScore);
            cdt.cancel();

            //show popup window
            popupBox();

        }
    return true; //super.onTouchEvent(event);
    }

    /**
     *
     */
    public void popupBox(){

        DatabaseHandler db = new DatabaseHandler(this);

        //if score > 0, then save in score table
        if(iScore > 0) {
            /**
             * CRUD Operations
             * */
            // Inserting Contacts
            Log.d("Insert: ", "Inserting ..");
            db.addScore(new LocalBestScores("Math", iScore));

            // Reading all contacts
            Log.d("Reading: ", "Reading all contacts..");
            List<LocalBestScores> scores = db.getScores();

            for (LocalBestScores scr : scores) {
                String log = "Id: "+ scr.get_id() +" ,Subject: " + scr.get_subject() + ", Score:" + scr.get_score() ;
                // Writing Contacts to log
                Log.d("Name: ", log);
            }
        }


        //show popup window
        AlertDialog alertDialog = new AlertDialog.Builder(SubjectMath.this).create();
        alertDialog.setTitle("Well done");
        alertDialog.setMessage("Your scored " + iScore + " points");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Replay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        startActivity(getIntent());
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Back To Main Menu",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }

}
