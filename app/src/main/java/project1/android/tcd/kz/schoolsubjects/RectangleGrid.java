package project1.android.tcd.kz.schoolsubjects;

/**
 * Created by kadirova on 25/11/2016.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import java.util.HashMap;

public class RectangleGrid extends View {

    private  Rect rectangle;
    private Paint paint;
    Boolean bTouch = false, //flag when onTouchEvent fired
            bInitColor = true; //square color initialisation

    private  static HashMap<String, Integer> RectColorArr=new HashMap<String, Integer>();

    float x1, y1; //onTouchEvent coordinates
    int[] squareColors = {0,0}; // for two colors only: GREEN,YELLOW
    String colorStatus = "GREEN: 0; YELLOW: 0";

    //initial vertex
    int x = 90;
    int y = 90;
    //int Xi= 100, Yi = 300;


    public RectangleGrid(final Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawColor(Color.LTGRAY);

        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++){

                // create a rectangle that we'll draw later
                int x11 = x * (i+1),
                        y11 = y * (j+1),
                        x12 = (i+2) * x,
                        y12 = y * (j+2);

                rectangle = new Rect( x11, y11, x12, y12);
                //System.out.println("" + j + "" + i +"] " + x11 + ", " + y11 + ", " + x12 + ", " + y12);

                // create the Paint and set its color
                paint = new Paint();
                //paint.setColor((System.currentTimeMillis()/1000) % 2 == 0? Color.WHITE: Color.GREEN);
                if(bInitColor)
                {
                    paint.setColor(Color.WHITE);
                    RectColorArr.put("" +i + ""+j, paint.getColor());
                }

                if(bTouch)
                {
                    if ( (x1 >= x11 && x1 <= x12) && (y1 >= y11 && y1 <= y12)) {
                        paint.setColor(nextColor(RectColorArr.get("" +i + ""+j)));
                        colorStatus = getSquareColors();
                        bTouch = false;
                        System.out.println("Color=" + paint.getColor());
                        RectColorArr.put("" +i + ""+j, paint.getColor());
                    }
                }


                int iC = RectColorArr.get("" +i + ""+j);
                System.out.println("key=" + "" +i + ""+j + ", value=" + iC);
                paint.setColor(iC);
                canvas.drawRect(rectangle, paint);

                // border
                //paint.setStrokeWidth(5);
                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(Color.BLUE);
                canvas.drawRect(rectangle, paint);

                Paint paint2= new Paint();
                paint2.setColor(Color.BLACK);
                paint2.setTextSize(50);  //set text size
                canvas.drawText("" + j + "" + i, x * (i+1), y * (j+2) ,paint2); //x=300,y=300
            }

        bInitColor = false;
    }


    /**
     * fires on touch
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //getting the touched x and y position
        x1 = event.getX();
        y1 = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                // create a rectangle that we'll draw later
                Rect rectangle1 = new Rect(x , y , 2*x, 2*y);
                //System.out.println(rectangle.toString());

                // create the Paint and set its color
                Paint paint1 = new Paint();
                paint1.setColor(Color.BLACK);

                new Canvas().drawRect(rectangle1, paint1);

                System.out.println("x1=" + x1 + ", y1=" + y1);
                bTouch = true;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                //Log.d("LOG","Move");
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                //Log.d("LOG", "Up");
                invalidate();
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * Picks the next color. The order is WHITE, GREEN, YELLOW, WHITE
     * @param color_
     * @return
     */
    public int nextColor(int color_){
        if( color_ == Color.WHITE )
        {
            squareColors[0]++; //add one for GREEN
            return Color.GREEN;
        }
        else if( color_ == Color.GREEN )
        {
            squareColors[0]= (squareColors[0] <= 0)? 0: squareColors[0] - 1;// extract one if GREEN > 0
            squareColors[1]++; //add one for YELLOW
            return Color.YELLOW;}
        else if( color_ == Color.YELLOW )
        {
            squareColors[1] --;// extract one if YELLOW
            return Color.WHITE;
        }
        return -1;
    }

    /**
     * return status of colors
     * @return
     */
    public String getSquareColors(){
        return "GREEN: " + squareColors[0] + ", YELLOW: " + squareColors[1];
    }


}


