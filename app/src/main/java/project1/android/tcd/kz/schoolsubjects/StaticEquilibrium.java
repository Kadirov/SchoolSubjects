package project1.android.tcd.kz.schoolsubjects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by kadirova on 26/11/2016.
 */

public class StaticEquilibrium extends View {

    int width = 10;
    int thickness = 40;
    int num = 100;

    int x0 = 100, xMax = x0 + num * width;
    int y0 = 100;

    int load1 = 80, load2 = 20;
    int pvtX =  xMax /2;

    boolean bDragged = false;

    int torque1 = 1, torque2 = 2;

    Bitmap bPivot = BitmapFactory.decodeResource(getResources(), R.drawable.pivot2);

    public StaticEquilibrium(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        // border
        Paint pntRod = new Paint();
        pntRod.setStyle(Paint.Style.STROKE);
        pntRod.setColor(Color.BLUE);

        Paint pntQuantizer= new Paint();
        pntQuantizer.setColor(Color.RED);
        pntQuantizer.setTextSize(20);  //set text size

        Paint pntLeftLine= new Paint();
        pntLeftLine.setColor(Color.GREEN);
        pntLeftLine.setTextSize(35);  //set text size

        Paint pntRightLine= new Paint();
        pntRightLine.setColor(Color.BLUE);
        pntRightLine.setTextSize(35);  //set text size

        Paint dashed = new Paint();
        dashed.setColor(Color.RED);
        dashed.setStyle(Paint.Style.STROKE);
        dashed.setPathEffect(new DashPathEffect(new float[]{2, 5, 7, 10}, 0));

        //draw the rod (20 thin rectangles)
        for (int i = 0; i < num; i++)
        {
            Rect rectangle = new Rect(x0 + i * width, y0, x0 + (i+1) * width, y0 + thickness);

            canvas.drawRect(rectangle, pntRod);

            //length markers {0,5,10,..}
            if( (i+1) % 5 == 0) {
                canvas.drawLine(x0 + (i+1) * width, y0 + thickness, x0 + (i+1) * width, y0 + 1.5f * thickness, pntQuantizer);
                canvas.drawText("" + (i+1), x0 + (i+1) * width, y0 + 2* thickness , pntQuantizer); //x=300,y=300
            }
        }
        //draw loads
        canvas.drawRect(x0 - 50, y0 - 75, x0 + 50, y0 - 25, pntRod );
        canvas.drawText( load1 + " KG", x0 - 25 , y0 - 50 , pntQuantizer); //x=300,y=300

        canvas.drawCircle(xMax, y0 - 75, 50, pntRod);
        canvas.drawText(load2 + " KG", xMax - 25, y0 - 50 , pntQuantizer); //x=300,y=300

        //draw pivot only first time
        canvas.drawLine( pvtX, y0 - 4 * thickness, pvtX, y0 + 4 * thickness, dashed);
        if (bDragged)
        {
            //left side line
            canvas.drawLine(x0, y0 - 15, pvtX, y0 - 15, pntLeftLine);
            canvas.drawText("" + (pvtX - x0), x0 + (pvtX - x0)/2, y0 - 15 , pntLeftLine); //x=300,y=300
            torque1 = load1 * (pvtX - x0);
            canvas.drawText("Torque 1 = " + load1 + " x " + (pvtX - x0) + " = " + torque1, x0 + (pvtX - x0)/2, y0 - 65 , pntLeftLine); //x=300,y=300
            //right side
            canvas.drawLine(pvtX, y0 - 15, xMax, y0 - 15, pntRightLine);
            canvas.drawText("" + (xMax - pvtX), pvtX + (xMax - pvtX)/2, y0 - 15 , pntRightLine); //x=300,y=300
            torque2 = load2 * (xMax - pvtX);
            canvas.drawText("Torque 2 = " + load2 + " x " + ((xMax - pvtX)) + " = " + torque2, pvtX + (xMax - pvtX)/2, y0 - 65 , pntRightLine); //x=300,y=300
        }
        canvas.drawBitmap(bPivot, pvtX, y0 + 3 * thickness, pntRod);

    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.
        System.out.println("Subclass");

        float x = event.getX();
        float y = event.getY();

        System.out.println("event.getAction()=" + event.getAction() + ", MotionEvent.ACTION_MOVE") ;

        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                System.out.println("2)  event.getAction()=" + event.getAction() + ", MotionEvent.ACTION_MOVE") ;



                // reverse direction of rotation above the mid-line
                if ( x > pvtX) {
                    pvtX = (x > xMax) ? xMax : (int) x ;
                }
                // reverse direction of rotation to left of the mid-line
                if (x < pvtX) {
                    pvtX = (x < x0 ) ? x0 : (int) x ;
                }
                invalidate();
                break;
        }


        bDragged = true;
        return  super.onTouchEvent(event);
    }

    public void dragPivot(MotionEvent event){
        float x = event.getX();
        float y = event.getY();

        System.out.println("2)  event.getAction()=" + event.getAction() + ", MotionEvent.ACTION_MOVE") ;

        // reverse direction of rotation above the mid-line
        if ( x > pvtX) {
            pvtX = (x > xMax) ? xMax : (int) x ;
        }
        // reverse direction of rotation to left of the mid-line
        if (x < pvtX) {
            pvtX = (x < x0 ) ? x0 : (int) x ;
        }
        invalidate();
    }

}
